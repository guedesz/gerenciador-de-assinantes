const mongoose = require('mongoose');

const subscriberSchema = new mongoose.Schema({
    id: {
        type: Number,
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
    },
    dateOfBirth: {
        type: Date,
        required: true
    },
    cellphone: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    state: {
        type: String,
        require: true
    },
    status: {
        type: Boolean,
        default: true,
        required: true
    }

} , { timestamps: true });

module.exports = mongoose.model('subscriber', subscriberSchema);